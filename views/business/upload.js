(() => {
  document.getElementById("file-input").onchange = () => {
    const files = document.getElementById('file-input').files;
    const file = files[0];
    console.log(files);
    if(file == null){
      return alert('No file selected.');
    }
    var readers = [];

    reminaingNoOfFiles = files.length;

    for (var index in files) {
      if (index < files.length) {
        /*var output = '';
        for (var property in files[index]) {
          output += '<div>'+property + ': ' + files[index][property]+'; </div> ';
        }
        $('#instruction').append(output+'<p> ------------- </p>');*/
        var funconload = function (e) {
            var elem = $('#imagecontainer').clone().attr('id','imagecontainer'+this.index);
            elem.attr('data-sort', this.index);
            elem.css('display','inline-block');
            $('#imagegrid').append(elem);
            $(elem).append('<div style="max-height:70px; height:70px;"> <textarea maxlength= "50" data-id="'+this.index+'"class="imageText" placeholder="أضف تعليق للصورة"></textarea> </div>')
            //elem.append('<span style="position:absolute; top:0px; right:0px; font-size: 30px; background: #ffffff99; padding: 5px; color: #ff000077;"> X </span>')
            //elem.append('<p>'+ files[this.index].size +'</p>')
            elem.find('.thumbnail').attr('src', e.target.result);
            //sort();
        }

        var obj = {index:parseInt(index)+parseInt(hindex)};
        var boundfunconload = funconload.bind(obj)
        var reader = new FileReader();
        reader.onload = boundfunconload;
        reader.readAsDataURL(files[index]);
        readers.push(reader);
        getSignedRequest(files[index], parseInt(index)+parseInt(hindex));
      }
    }
    hindex = hindex + files.length; // allows multi button press 

  };
})();

var reminaingNoOfFiles;
var hindex = 0;
var imageURL = [];

var inv = $('#invId').html();

function getSignedRequest(file, index){
  const xhr = new XMLHttpRequest();
  xhr.open('GET', `/business/sign-s3?file-name=${file.name}&file-type=${file.type}&invid=${inv}`);
  xhr.onreadystatechange = () => {
    if(xhr.readyState === 4){
      if(xhr.status === 200){
        const response = JSON.parse(xhr.responseText);
        imageURL[index] = response.fileName;
        //$('#imageText'+index).attr('data-s3id', response.fileName)
        uploadFile(file, response.signedRequest, response.url, index);
      }
      else{
        alert('Could not get signed URL.');
      }
    }
  };
  xhr.send();
}


function uploadFile(file, signedRequest, url, index){
  const xhr = new XMLHttpRequest();
  xhr.open('PUT', signedRequest);
  var obj = {index:index};
  var bounduploadprogress = uploadProgress.bind(obj)
  xhr.upload.addEventListener("progress", bounduploadprogress, false);
  xhr.onreadystatechange = () => {
    if(xhr.readyState === 4){
      if(xhr.status === 200){
        $('#imageid').val(imageURL);
      }
      else{
        alert('Could not upload file.');
      }
    }
  };
  xhr.send(file);
}

var percentCompletePrevious = [];

function uploadProgress(evt) {
  if (evt.lengthComputable) {
    var percentComplete = Math.round(evt.loaded * 100 / evt.total);
    //console.log(this.index, ':',percentComplete / 100);
    //console.log(this.index)
    $('#imagecontainer'+this.index).find('.circle').circleProgress({
      value: percentComplete / 100,
      size: 80,
      fill: {
        gradient: ["green", "lightgreen"]
      },
      animationStartValue: percentCompletePrevious[this.index]
    });
    percentCompletePrevious[this.index] = percentComplete / 100;
    if (percentComplete == 100) {
      reminaingNoOfFiles = reminaingNoOfFiles - 1;
      console.log(reminaingNoOfFiles)
      if (reminaingNoOfFiles == 0) {
        $('button[type=submit]').prop('disabled', false);
      }
      $('#imagecontainer'+this.index).find('.checkmark').css('display', 'inherit')
    }

  }
} 


$(document).ready(function() {

  beforeSubmit();

  $('.row').click(function() {
    if ($(this).find('input').length > 1 || $(this).find('textarea').length > 1 ) {
      return;
    }
    $(this).find('input').focus();
    $(this).find('textarea').first().focus();
    //console.log($(this).find('input'))
  })
  //$('#details').characterCounter();
  //$('select').material_select();

  $('.modal').modal({
      inDuration: 300, // Transition in duration
      outDuration: 200, // Transition out duration
      endingTop: '1%', // Ending top style attribute
  });

  /*$('#duedate').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 10, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    close: 'Ok',
    closeOnSelect: true // Close upon selecting a date,
  });*/


  $('#upload').bind("click",function() 
    { 
        var imgVal = $('#uploadfile').val(); 
        if(imgVal=='') 
        { 
            alert("empty input file"); 
            return false; 
        } 
    }); 
});

function sort() {
  var elems = $('.imagecontainer').sort(function (a, b) {
    var contentA =parseInt( $(a).attr('data-sort'));
    var contentB =parseInt( $(b).attr('data-sort'));
    return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
  })
  elems.detach().appendTo('#imagegrid');
}

$('#preview').on('click', function() {
  $('#modal1').modal('open');
  var typeEstate = $('input[name="group1"]:checked').val()===undefined ? '' : $('input[name="group1"]:checked').val(); 
  $('#tbody').empty();
  $('#tbody').append('<tr> <td> المنطقة </td> <td> '+$('input[name="region"]').val()+' </td> </tr>');
  $('#tbody').append('<tr> <td> نوع العقار </td> <td> '+typeEstate+' </td> </tr>');
  $('#tbody').append('<tr> <td> المساحة </td> <td> '+$('input[name="area"]').val()+' </td> </tr>');
  $('#tbody').append('<tr> <td> السعر </td> <td> '+$('input[name="price"]').val()+' (ريال) </td> </tr>');
  $('#tbody').append('<tr> <td> التفاصيل </td> <td> '+$('textarea[name="details"]').val()+' </td> </tr>');
  $('#tbody').append('<tr> <td> لــ </td> <td> '+$('input[name="group2"]:checked').val()+' </td> </tr>');

})

function parseArabic(str) {
    return Number( str.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(d) {
        return d.charCodeAt(0) - 1632; // Convert Arabic numbers
    }).replace(/[۰۱۲۳۴۵۶۷۸۹]/g, function(d) {
        return d.charCodeAt(0) - 1776; // Convert Persian numbers
    }) );
}


var imageTextArray = {};

function beforeSubmit() { 
  var form = document.getElementById('NewPropertyForm');

  if (form) {
    form.addEventListener('submit', function(e) {
      var fdata = new FormData(this);
      fdata.append('price', parseArabic($('#price').val()));
      fdata.append('area', parseArabic($('#area').val()));
      /*for (var p of fdata) {
        console.log(p);
      }*/
      ajaxFileUpload(fdata);
      event.preventDefault();
    }, false);
  }

}


var ajaxFileUpload = function(data) {

    var imageTextLen = $('.imageText').length;
    var images = $('.imageText'); 
    if (imageTextLen > 0) {
      for (var i = 0; i<imageTextLen; i++) {
        if (images[i].value.length > 4) {
          imageTextArray[i] = images[i].value;
        }
      }
    }

    data.append('imageText', JSON.stringify(imageTextArray))


    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/business/upload?invid='+inv, true);
    xhr.addEventListener('load', function(e) {
        //console.log(xhr.responseText);
        grecaptcha.reset();
        if (xhr.status == 200) {
          var sid = xhr.responseText;
          //window.open('/customer/s/'+sid, '_blank');
          //navigate('/customer/s/'+sid, true);
          window.location = '/customer/s/'+sid;
          return;
        }
        var data = JSON.parse(xhr.responseText).errors;
        if (data) {
          $('#errorsList').empty();
          $('#errors').css('display','block');
          $("html, body").animate({ scrollTop: 0 }, "slow");
          //$(window).scrollTop(0);
          for (var i in data) {
            console.log(data[i])
            $('#errorsList').append('<li>'+data[i].msg+'</li>');
          }
        }
    });
    xhr.send(data);
};

function navigate(href, newTab) {
   var a = document.createElement('a');
   a.href = href;
   if (newTab) {
      a.setAttribute('target', '_blank');
   }
   a.click();
}

function onHuman() {
  var form = document.getElementById('NewPropertyForm');

  if (form) {
      var fdata = new FormData(form);
      fdata.append('price', parseArabic($('#price').val()));
      fdata.append('area', parseArabic($('#area').val()));
      /*for (var p of fdata) {
        console.log(p);
      }*/
      ajaxFileUpload(fdata);
      event.preventDefault();
  }
}

/*function expiredFunc() {
  grecaptcha.reset();
}*/