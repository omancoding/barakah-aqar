function onHuman(response) { 
  console.log(response);
  //document.getElementById('captcha').value = response;
  //e.preventDefault();
  $.ajax({
    type: 'POST',
    url: '/customer/alerts/subscribe',
    data: $('#subscribeForm').serialize(),
    success: function(response) {
      //console.log(response);
      //alert(response)
      $('#subscribeForm').css('display', 'none');
      $('#check').css('display', 'block');
    },
    error: function(response) {
      console.log(response.responseText);
      grecaptcha.reset();
      var errors = JSON.parse(response.responseText).errors;
      $('#error').html(errors['mobilenumberSubscribe'].msg);
    }
  });   
} 

$(document).ready(function() {
  $('#mobileSubscribe').focus(function(){
    $(this).attr('placeholder','')
    $(this).css('direction','ltr')
  });

  $('#mobileSubscribe').keyup(function(){
    $(this).val($(this).val().toString().toEnglishNumbers())
  });

});

String.prototype.toIndiaDigits= function(){
 var id= ['۰','۱','۲','۳','٤','٥','٦','۷','۸','۹'];
 return this.replace(/[0-9]/g, function(w){
  return id[+w]
 });
}

String.prototype.toEnglishNumbers = function() {
    return this.replace(/[٠١٢٣٤٥٦٧٨٩]/g, function(d) {
        return d.charCodeAt(0) - 1632; // Convert Arabic numbers
    })
}

