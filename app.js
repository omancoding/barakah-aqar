var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var uuidv4 = require('uuid/v4');
var mongoose = require ('mongoose');
var session = require('express-session');
require('dotenv').config()
var MongoStore = require('connect-mongo')(session);

var routes = require('./routes/index');
var users = require('./routes/users');
var business = require('./routes/business');
var customer = require('./routes/customer');
var admin = require('./routes/admin');
var sslRedirect = require('heroku-ssl-redirect');
var useragent = require('express-useragent');



var app = express();
// enable ssl redirect
app.use(sslRedirect());
/*app.use(function (req, res, next) {
  if (app.get('env') !== 'development') {
    !req.secure ? res.redirect('https://' + req.hostname + req.url) : next()
  } else {
    next();
  }
});*/
app.use(useragent.express());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')))
app.use(require('express-session')({
  genid: function(req) { var uid = uuidv4(); return uid; /* use UUIDs for session IDs*/},
  secret: 'fatoorasmssecret', 
  name:'user', 
  resave: false, 
  saveUninitialized: false,
  store: new MongoStore({ mongooseConnection: mongoose.connection })
 }));

app.use(passport.initialize());
app.use(passport.session());


app.use(function(req, res, next) {
  res.locals.user = req.user;
  next();
});


app.use('/', routes);
app.use('/users', users);
app.use('/business', business);
app.use('/customer',customer);
app.use('/admin',permit('admin'), admin);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

function permit() {
  for (var _len = arguments.length, allowed = Array(_len), _key = 0; _key < _len; _key++) {
    allowed[_key] = arguments[_key];
  }

  var isAllowed = function isAllowed(role) {
    return allowed.indexOf(role) > -1;
  };

  // return a middleware
  return function (req, res, next) {
    if (req.user && isAllowed(req.user.role)) next(); // role is allowed, so continue on the next middleware
    else {
        //res.status(403).json({ message: "Forbidden" }); // user is forbidden
        res.redirect('/login');
    }
  };
}


module.exports = app;
