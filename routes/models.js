var mongoose = require ('mongoose');

var userSchema = new mongoose.Schema({
  name: { type: String, trim: true },
  id: { type: String, unique: true},
  password: { type: String},
  role: {type: String},
  other: {}
});

var estatePropSchema = new mongoose.Schema({
  sid: { type: String},
  region: { type: String},
  type: {type: String},
  offer: {type: String},
  area: {type: Number},
  price: {type: Number},
  details: {type: String},
  images: {type: Array},
  imageText: {type: String}, // irregular
  noOfViews: {type: Number, default: 0},
  noOfExtns: {type: Number, default: 0},
  location: {type: String},
  user: {type: String},
  noOfDataReq: {type: Number}, // including WA and mobile - records purpose
  score: {type: Number, default: 0},
  userApproved: {type: Boolean, default: false},
  suspended: {type: Boolean, default: false},
  visitors: {type: Array},
  created_at : { type: Date, default: Date.now },
});

var invitationSchema = new mongoose.Schema({
  invid: { type: String},
  consumed: {type: Boolean, default: false},
  location: {type: String},
  user: {type: String},
  images: {type: Array},
  sid: {type: String},
  created_at : { type: Date, default: Date.now },
});


var subscriberSchema = new mongoose.Schema({
  mobile: {type: String},
  ip: {type: String},
  created_at : { type: Date, default: Date.now },
});

var potentialSchema = new mongoose.Schema({
  mobile: {type: String},
  created_at : { type: Date, default: Date.now },
});




var User = mongoose.model('users', userSchema);
var Property = mongoose.model('properties', estatePropSchema);
var Invitation = mongoose.model('invitations', invitationSchema);
var Subscriber = mongoose.model('subscribers', subscriberSchema);
var Potential = mongoose.model('potentials', potentialSchema);


module.exports = {
 user: User,
 property: Property,
 invitation: Invitation,
 subscriber: Subscriber,
 potential: Potential
};