var express = require('express');
var aws = require('aws-sdk');
var formidable = require('formidable');

var router = express.Router();
var uuidv4 = require('uuid/v4');
const { body,validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

var models = require('./models.js');


/*client.messages.create(
  {
    from: '+18182084922',
    to: '+96894033945',
    body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
  },
  (err, message) => {
    if (err)
      console.log(err)
    else
    console.log(message.sid);
  }
);*/


router.get('/upload', function(req, res, next) {
  res.render('invite');
});

router.get('/', function(req, res, next) {
  //res.redirect('/admin/upload');
  res.render('admin_panel')
});


router.post('/upload', function(req, res, next) {
  /*var form = new formidable.IncomingForm();
  form.parse(req, function(err, fields, files) {
    req.body = fields;
    console.log(req.body)
  });*/
  console.log(req.body);
  var sid = uuidv4(); 
  var Invitation = new models.invitation ({
    invid: sid,
    user: req.body.mobile,
    images: req.body.imageid.split(','),
  });

  Invitation.save(function (err) {
    if (err) {
      console.log ('Error on save!', err); 
      return res.status(500);
    }
    //return res.status(200).send('Invitation created!');
    res.redirect('/customer/page/all')
  });
});


router.get('/addLandlord', function(req, res, next) {
  var whatsapp = '*السلام عليكم* \n \
  *هل تملك عقاراً أو مجموعة من العقارات؟ هل تريد أن تؤجر أو تبيع؟*\n\
قم بذلك مجاناً مع البركة للعقار.\n\
من أهم ميزات الموقع: \n\
  • يمكنك رفع حتى ١٠٠ صورة لعقارك. \n\
  • وضع تعليق على الصور. \n\
  • امكانية تواصل الزبائن مع مالك العقار مباشرةً بطريقة سهلة *مع حماية الرقم*. \n\
  • امكانية مشاركة المعروض مع الآخرين.\n\
  • يظل المعروض إلى أن يلغيه صاحب المعروض.\n\
  • و غيرها من الميزات . \n\
  أضف عقارك هنا *مجاناً* \n\
  https://barakah.cc/business/upload \n \
  أو بإمكانك ارسال التفاصيل عبر الرد على هذه الرسالة *و سنقوم نحن بإضافته لك*\n \
  \
  مثال لمعروض:\n  \
  http://barakah.cc/customer/s/06491dac-67e0-4fb9-841a-755f7b500d21 '
  models.invitation.find({}, function(err, invs) {
    models.property.find({}, function(err, properties) {
      models.potential.find({}, function(err, potentials) {
        res.render('page',{invs: invs, properties:properties, potentials: potentials, wt: encodeURIComponent(whatsapp)});  
      });
    });
  });
});


router.post('/addLandlord' , function (req, res, next) {

  var Potential = new models.potential ({
    mobile: req.body.mobile,
  });

  Potential.save(function (err) {
    if (err) { 
      console.log ('Error on save!', err); 
      res.status(500).send(err);
      return;
    } else {
      //res.status(200).send('Ok!');
      res.redirect(req.originalUrl);
    }
  });
});



module.exports = router;
