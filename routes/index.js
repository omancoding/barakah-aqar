var express = require('express');
var router = express.Router();
var mongoose = require ('mongoose');
var passport = require('passport');
var Strategy = require('passport-local').Strategy;
var models = require('./models.js');


var uristring =  process.env.MONGODB_URI
mongoose.connect(uristring, function (err, res) {
  if (err) { 
    console.log ('ERROR connecting to: ' + uristring + '. ' + err);
  } else {
    console.log ('Succeeded connected to: ' + uristring);
  }
});

//shared
String.prototype.toIndiaDigits= function(){
 var id= ['۰','۱','۲','۳','٤','٥','٦','۷','۸','۹'];
 return this.replace(/[0-9]/g, function(w){
  return id[+w]
 });
}


var User = models.user;

passport.use(new Strategy(
  function(username, password, cb) {
	User.findOne({ id: username}, function (err, user){
      if (err) { return cb(err); }
      if (!user) { return cb(null, false); }
      if (user.password != password) { return cb(null, false); }
      return cb(null, user);
    });
  }));

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  User.findOne({ id: id}, function (err, user){
    if (err) { return cb(err); }
    //console.log(user)
    cb(null, user);
  });
});

//************************************************************************************
//------------------------------------------------------------------------------------
//************************************************************************************


router.get('/.well-known/acme-challenge/:content', function(req, res) {
  res.send('3xWUATSalvnwQUcBCJjpqgvS50vG9yZTP7RVAttnka8.-LFzz0jd9rwHflSiPhmeCQBZ2c0o_A-qWwIYOkv_76Q')
})

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/login', function(req, res, next) {
  //seed();	
  res.render('login');
});

router.post('/login', 
  passport.authenticate('local', { failureRedirect: '/login' }),
  function(req, res) {
  	//res.send('auth is ok');
    //res.locals.user = req.user; does not work
    res.redirect('/admin');
});


router.get('/logout',
  function(req, res){
    req.logout();
    res.redirect('/');
});


function seed() {

	// Clear out old data
	models.user.remove({}, function(err) {
	  if (err) {
	    console.log ('error deleting old data.');
	  }
	});

	// Creating one user.
	var user1 = new models.user ({
	  name: 'user1',
	  id: '1000',
	  password: 'easy',
	  role: 'admin'
	});

	// Saving it to the database.  
	user1.save(function (err) {if (err) console.log ('Error on save!', err)});
	
	// Creating one user.
	var user2 = new models.user ({
	  name: 'user2',
	  id: '2000',
	  password: 'easy',
	  role: 'admin'
	});

	// Saving it to the database.  
	user2.save(function (err) {if (err) console.log ('Error on save!', err)});

}

module.exports = router;
