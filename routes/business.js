var express = require('express');
var aws = require('aws-sdk');
var formidable = require('formidable');
var router = express.Router();
var uuidv4 = require('uuid/v4');
var xssFilters = require('xss-filters');
const { body,validationResult } = require('express-validator/check');
//const { sanitizeBody } = require('express-validator/filter');

var Recaptcha = require('express-recaptcha');
var models = require('./models.js');

//var recaptcha = new Recaptcha(process.env.GOOGLE_CAPTCHA_SITE_NEW_PROP, process.env.GOOGLE_CAPTCHA_SECRET_NEW_PROP);
var recaptcha = new Recaptcha(process.env.GOOGLE_CAPTCHA_SITE, process.env.GOOGLE_CAPTCHA_SECRET);

var S3_BUCKET = process.env.S3_BUCKET;


/*client.messages.create(
  {
    from: '+18182084922',
    to: '+96894033945',
    body: 'This is the ship that made the Kessel Run in fourteen parsecs?',
  },
  (err, message) => {
    if (err)
      console.log(err)
    else
    console.log(message.sid);
  }
);*/


router.get('/upload', /*invidChecker,*/ function(req, res, next) {
  var invid;
  if (req.query) {
    if (req.query.invid) {
      invid = req.query.invid
    }
  }
  res.render('business/upload', {data: {invid: invid}});
});


function imageChecker(val, {req, loc, path}) {
  var length = val.split(',').length;
  if (length < 7) {
    if (req.body.group1 == 'أرض') {
      if (length < 3) {
        throw new Error('الصور:حتى يكون معروضك واضحاً و يحمل أكبر قدر من المعلومات للزبائن، تنص سياسة الموقع على أن عدد الصور يجب أن لا يقل عن(٧) لأي نوع من العقار، ما عدا الأرض فيسمح بأقل عدد هو (٣).')
      } else {
        return val;
      }
    } else {
      throw new Error('الصور: حتى يكون معروضك واضحاً و يحمل أكبر قدر من المعلومات للزبائن، تنص سياسة الموقع على أن عدد الصور يجب أن لا يقل عن(٧) لأي نوع من العقار، ما عدا الأرض فيسمح بأقل عدد هو (٣).')
    }
  } else {
    return val;
  }
}

function invidChecker(req, res, next) {
  if (req.user) {
    return next(); // return is necessary
  }
  var invid;
  if (req.query) {
    invid = req.query.invid || null;
  }

  models.invitation.findOne({ 'invid': invid }, function (err, inv) {
    if (err) return err;
    console.log('invitaion auth:',inv)
    if (inv) {
      var sid = inv.sid;
      if (!inv.consumed) {
        req.inv = inv;
        next(); 
      } else {
        //res.send('The invitation has expired');
        res.redirect('/customer/s/'+sid);
      }
    } else {
      //console.log('The invitation does not exist')
      res.send('The invitation does not exist');
    }
  });
}

router.post('/upload', function(req, res, next) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
      req.body = fields;
      next();
    });
  }, /*invidChecker, */ [
    body(),
    body('region', 'المنطقة:  على الأقل ٥ أحرف و لا يزيد عن ٢٠٠ حرف.').isLength({ min: 5, max:200 }).trim(),/*.isAlpha(['ar']).withMessage('المنطقة:   يجب أن يكون باللغة العربية و تشتمل على الحروف فقط.'),*/
    body('group1', 'Invalid type for the property').isIn(['شقة','بيت', 'فيلا','ملحق', 'محل', 'مكتب','معرض','مخزن','أرض']),
    body('area', 'المساحة: أقل مساحة هي ١ (م٢) و أكثر مساحة هي ١٠٠,٠٠٠ (م٢).')
      .isLength({ min: 1, max: 6 }).not().equals('0')
      .isNumeric().withMessage('المساحة: فقط الأرقام مسموحة.'),
    body('price', 'السعر: أقل سعر هو (١) ريال و أكثر سعر هو (١,٠٠٠,٠٠٠) ريال.')
      .isLength({ min: 1, max: 7 }).not().equals('0')
      .isNumeric().withMessage('السعر: فقط الأرقام مسموحة.'),
    body('details', 'التفاصيل: على الأقل ٥٠ حرفاً و لا يزيد عن ٥٠٠٠ حرف.').isLength({ min: 50,  max: 5000 }).trim(),
    body('group2', 'Invalid offer').isIn(['بيع','إيجار']),
    body('imageText','تعليقات الصور: تجاوزت العدد المسموح من الأحرف.').isLength({max:3000}),
    body('imageid','الصور: يجب أن تكون هناك صوراً.').isLength({min:10, max:3000}).custom(imageChecker),
    body('mobilenumber', 'رقم الجوال: يجب أن يتكون من ٧ أو ٨ أرقام.')
      .isLength({ min: 7, max: 8 })
      .isNumeric().withMessage('رقم الجوال: فقط الأرقام مسموحة.'),    // sanitzation required -- test it with html later
  ],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.status(300).send({ errors: errors.mapped() });
    } else {
      next();
    }
  },
    recaptcha.middleware.verify, function (req, res, next) {
      if (!req.recaptcha.error) {
        next();
      } else {
        res.status(300).send({ errors: {'0': {msg:'الرجاء التحقق من  أنك لا تدخل البيانات آلياً و ذلك بالضغط على المربع  الذي يأتي  بعد حقل الجوال.'}} });
      }

  },
  (req, res, next) => {
    var sid = uuidv4(); 
    //var mobile = req.inv ? req.inv.mobile : '94033945';
    var mobile = req.body.mobilenumber
		var Property = new models.property ({
		  sid: sid,
		  region: xssFilters.inHTMLData(req.body.region),
		  type: req.body.group1,
		  offer: req.body.group2,
		  area: req.body.area,
		  price: req.body.price,
      images: req.body.imageid.split(','),
      imageText: xssFilters.inHTMLData(req.body.imageText),
      details: xssFilters.inHTMLData(req.body.details),
      user: mobile,
		});
    // make sure that you approve if it comes from invitation
    // set userApproved=true
    // set the invitation consumed to true
    // copy other information like the owner and location


		Property.save(function (err) {
			if (err) { 
        console.log ('Error on save!', err); 
        res.status(500).send(err);
        return;
      }
      if (req.inv) {
        req.inv.consumed = true;
        req.inv.sid = sid;

        req.inv.save(function(err) {
          if (err) { 
            console.log ('Error on save!', err); 
            res.status(500).send(err);
          } else { 
            res.status(200).send(sid);
          }
        })
      } else {
        res.status(200).send(sid);
      }
		});
  }
);

router.get('/sign-s3', /*invidChecker,*/  function(req, res, next) {
  const s3 = new aws.S3();
  //const fileName = req.query['file-name'];
  const fileName = uuidv4();
  const fileType = req.query['file-type'];
  const s3Params = {
    Bucket: S3_BUCKET,
    Key: fileName,
    //CacheControl: 'max-age=3456000, public', // doesn't work in Chrome, FF & Safari do return 304 but not as shown here
    ContentType: fileType,
    ACL: 'public-read',

  };

  s3.getSignedUrl('putObject', s3Params, (err, data) => {
    if(err){
      console.log(err);
      return res.end();
    }
    const returnData = {
      signedRequest: data,
      url: `https://${S3_BUCKET}.s3.amazonaws.com/${fileName}`,
      fileName: fileName
    };
    res.write(JSON.stringify(returnData));
    res.end();
  });
});



module.exports = router;
