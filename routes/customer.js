var express = require('express');
var url = require('url');
var router = express.Router();
var Recaptcha = require('express-recaptcha');
var models = require('./models.js');
const { body,validationResult } = require('express-validator/check');

var recaptcha = new Recaptcha(process.env.GOOGLE_CAPTCHA_SITE, process.env.GOOGLE_CAPTCHA_SECRET);


router.get('/s/:sid', function(req, res, next) {
  // check the sid to know if the property is allowd (both approved and not suspened)
  // get the info, display it table then images (use revealscroll) if possible
  // when end reached mark it as viewd, or at least when 3 images are seen
  // make the whatsapp share and the recaptcha (which needs another url)

  function fullUrl(req) {
    return url.format({
      protocol: req.protocol,
      host: req.get('host'),
      pathname: req.originalUrl
    });
  }

  //console.log(encodeURIComponent(fullUrl(req)));

  models.property.findOne({ 'sid': req.params.sid }, function (err, property) {
    if (err) return err;
    //console.log(property);
    if (!property) {
      return res.redirect('/');
    }
    var details = property.details; //+ '\n\n*تنويه:* \n موقع البركة للعقار هو منصة لإعلانات العقار [مجاناً] حيث يقوم ملاّك العقار بعرض عقاراتهم مباشرةً و لا يعتبر الموقع وسيطاً أو مسؤولاً عن المعروض أو البيانات المعروضة.'
    if (property) {
      var metaData = {
        ogimage: req.protocol+'://'+req.get('host')+'/images/barakah.png',//'https://s3-us-west-2.amazonaws.com/fatoorasms/'+property.images[0],
        ogtitle: 'البركة للعقار | '+property.type,
        ogdescription: '*للـ'+property.offer +'* - '+property.region+' - \n '+property.area+' م٢  - \n '+property.price+' ريال',
        title: 'البركة للعقار'
      }
      res.render('customer', {data: property, url: process.env.S3_URL, meta: metaData, whatsappText: encodeURIComponent('\n'+details+'\n'+fullUrl(req)+'\n')});
    } else {
      res.status(450).send('This property not found');
    }
  });

});



router.get('/viewed/:sid', function(req, res, next) {

  var ipAddr = req.headers["x-forwarded-for"];
  if (ipAddr){
    var list = ipAddr.split(",");
    ipAddr = list[list.length-1];
  } else {
    ipAddr = req.connection.remoteAddress;
  }

  var conditions = { 'sid': req.params.sid },
     // update = { $inc: { noOfViews: 1 }, $push: {visitors: {ip: ipAddr, ua: req.useragent, date: new Date()}}};
     update = { $inc: { noOfViews: 1 } };
     
  if(req.useragent.isAuthoritative) {
    models.property.update(conditions, update, callback);
  }

  function callback (err, numAffected) {
    res.send('Ok');
    // numAffected is the number of updated documents
  };

});


router.post('/captcha/ownerdata/:sid' , recaptcha.middleware.verify,  function(req, res, next) {
    //console.log(req.body, 'Captcha ',req.recaptcha)
    if (!req.recaptcha.error) {
      var sid = req.params.sid;
      models.property.findOne({ 'sid': req.params.sid }, function (err, property) {
        if (err) return err;
        res.send(property.user || '94033945');
      });
     } else {
      res.status(300).send('Captcha not approved');
    }
});

router.post('/alerts/subscribe' , recaptcha.middleware.verify,
    body('mobilenumberSubscribe', 'رقم الجوال: يجب أن يتكون من ٧ أو ٨ أرقام.')
      .isLength({ min: 7, max: 8 })
      .isNumeric().withMessage('رقم الجوال: فقط الأرقام مسموحة.'),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        res.status(300).send({ errors: errors.mapped() });
      } else {
        next();
      }
    },
    function(req, res, next) {
      //console.log(req.body, 'Captcha ',req.recaptcha)
      if (!req.recaptcha.error) {

        var ipAddr = req.headers["x-forwarded-for"];
        if (ipAddr){
          var list = ipAddr.split(",");
          ipAddr = list[list.length-1];
        } else {
          ipAddr = req.connection.remoteAddress;
        }

        var Subscriber = new models.subscriber ({
          mobile: req.body.mobilenumberSubscribe,
          ip: ipAddr
        });




        Subscriber.save(function (err) {
          if (err) { 
            console.log ('Error on save!', err); 
            res.status(500).send(err);
            return;
          } else {
            res.status(200).send('Ok!');
          }
        });
      } else {
        res.status(300).send('Captcha not approved');
      }
});





module.exports = router;
